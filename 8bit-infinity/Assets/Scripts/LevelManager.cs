﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public string currentLevel;
    private GameObject[] _walls;
    [SerializeField]private Image _brightImage;
    [SerializeField]private Image _fadeImage;
    [SerializeField]private Text _text;
    private const string TRANSITION_TEXT = "";

    private Vector2 _topPos; 
    public Vector2 TopPos{
        get{return _topPos;}
    }
    private Vector2 _bottomPos; 
     public Vector2 BottomPos{
        get{return _bottomPos;}
    }
    private Vector2 _rightPos; 
     public Vector2 RighPos{
        get{return _rightPos;}
    }
    private Vector2 _leftPos; 
     public Vector2 LeftPos{
        get{return _leftPos;}
    }

    void Awake()
    {
        instance = this;
    }


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
       
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        
        if(MainManager.instance.currentScene != Utils.SCENE_MENU){
            // _brightImage.gameObject.SetActive(true);
            
            _text.text = TRANSITION_TEXT + currentLevel;
            StartCoroutine(WaitAndBrigthen());
        }
    }


    void Start()
    {
        _walls = GameObject.FindGameObjectsWithTag("RayWalls");

        foreach(GameObject wall in _walls){
            if(wall.transform.position.x > _rightPos.x){
                _rightPos = wall.transform.position;
            }
            if(wall.transform.position.x < _leftPos.x){
                _leftPos = wall.transform.position;
            }
            if(wall.transform.position.y > _topPos.y){
                _topPos = wall.transform.position;
            }
            if(wall.transform.position.y < _bottomPos.y){
                _bottomPos = wall.transform.position;
            }
        }
        // Debug.Log(_rightPos +" "+ _leftPos+" "+  _topPos+" "+ _bottomPos);
        MainManager.instance.currentLevel = currentLevel;
    }

    IEnumerator WaitAndBrigthen(){
        ChangeOpacity(1, true, _brightImage);
        yield return new WaitForSeconds(1f);
        float opacity = 1f;
        while(opacity > 0){
            opacity -= .01f;
            ChangeOpacity(opacity, true, _brightImage);
            yield return new WaitForFixedUpdate();
        }   
    }
    
    IEnumerator FadeAndLoadScene(string sceneName){
        ChangeOpacity(0, true, _brightImage);
        float opacity = 0f;
        while(opacity < 1){
            opacity += .02f;
            ChangeOpacity(opacity, false, _fadeImage);
         
            yield return new WaitForFixedUpdate();
        }
        float textOpacity = 0;
        if(opacity >= 1){
            while(textOpacity < 1){
                Debug.Log(textOpacity);
                textOpacity += .02f;
                ChangeOpacity(textOpacity, true, _brightImage);
                yield return new WaitForFixedUpdate();
            }
        }
        if(textOpacity >= 1){
            yield return new WaitForSeconds(1f);
            ChangeScene(sceneName);
        }
        
    }

    private void ChangeOpacity(float opacity, bool hasText, Image image){
            image.gameObject.SetActive(true);
            Color imgColor = image.color;
            imgColor.a = opacity;
            image.color = imgColor;
            if(hasText){
                ChangeTextOpacity(opacity);
            }
    }
    private void ChangeTextOpacity(float opacity){
            _text.gameObject.SetActive(true);
            Color imgColor = _text.color;
            imgColor.a = opacity;
            _text.color = imgColor; 
            
    }

    public void EndLevel(string sceneName){
     
        // _fadeImage.gameObject.SetActive(true);
        // _text.gameObject.SetActive(true);
        ChangeOpacity(0, true, _fadeImage);
        string destination = FindObjectOfType<ShadowDoor>()?.Destination;
        if(destination != null && (destination == Utils.SCENE_MENU || destination == Utils.SCENE_END)){
            _text.text = TRANSITION_TEXT + destination;
        }else{
            _text.text = TRANSITION_TEXT + ((currentLevel == Utils.SCENE_MENU)?1:int.Parse(currentLevel)+1);
        }
        StartCoroutine(FadeAndLoadScene(sceneName));

    }

    public void playAudio(AudioClip sound){
        MainManager.instance.PlayAudioInst(sound);
    }

    public void ChangeScene(string sceneName){
        MainManager.instance.currentScene = sceneName;
        MainManager.instance.LoadScene(sceneName);
    }

    IEnumerator WaitAndDo(float seconds){
        yield return new WaitForSeconds(seconds);
    }

    // called when the game is terminated
    void OnDisable()
    {
        // Debug.Log("OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
