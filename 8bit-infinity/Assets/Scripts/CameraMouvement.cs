﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouvement : MonoBehaviour
{

    private LevelManager _levelManager;
    private GameObject _player; 
    private Vector3 _velocity; 
    [SerializeField]private Vector3 _offset; 

    private Camera _camera;
    private float _height;
    private float _width;
    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
        _height = 2f * _camera.orthographicSize;
        _width =  _height * _camera.aspect;

        _levelManager = LevelManager.instance;
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 current = new Vector3(transform.position.x, transform.position.y, -10);
        Vector3 target = new Vector3(Mathf.Clamp(_player.transform.position.x,_levelManager.LeftPos.x + _width/2, _levelManager.RighPos.x - _width/2),
        Mathf.Clamp(_player.transform.position.y, _levelManager.BottomPos.y + _height/2, _levelManager.TopPos.y - _height/2), -10) + _offset;
        transform.position = Vector3.SmoothDamp(current, target, ref _velocity, .3f, 10f );
    }
}
