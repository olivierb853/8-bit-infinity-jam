 using UnityEngine;
 using System;
 using System.Collections;

 public static class Utils
 {

     public static string SCENE_MENU = "Menu";
     public static string SCENE_END = "End";
     public static string SCENE_LEVEL_01 = "Level_01";
     public static string SCENE_LEVEL_02 = "Level_02";
     public static string SCENE_LEVEL_03 = "Level_03";
     public static string SCENE_LEVEL_04 = "Level_04";
     public static string SCENE_LEVEL_05 = "Level_05";
     public static string SCENE_LEVEL_06 = "Level_06";
     public static string SCENE_LEVEL_07 = "Level_07";
     public static string SCENE_LEVEL_08 = "Level_08";
     public static string SCENE_LEVEL_09 = "Level_09";
     public static string SCENE_LEVEL_10 = "Level_10";
     public static string SCENE_LEVEL_11 = "Level_11";

     public static Vector2[] toVector2Array (this Vector3[] v3)
     {
         return System.Array.ConvertAll<Vector3, Vector2> (v3, getV3fromV2);
     }
         
     public static Vector2 getV3fromV2 (Vector3 v3)
     {
         return new Vector2 (v3.x, v3.y);
     }
 }