﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShadowDoor : ShadowWall 
{
[SerializeField]private List<GameObject> _keys;
[SerializeField]private string _destination;
public string Destination{
    get{return _destination;}
}
[SerializeField]private AudioClip _unlockedSound;
[SerializeField]private AudioSource _audio;
private bool _isUnlocked;
private bool _doubleCheck = true;


   public override void OnTriggerEnter2D(Collider2D other) {
        base.OnTriggerEnter2D(other);
        //  TODO: Empecher de detecter lorsqu'il est dans son etat inactif.
       
    }
    private void OnTriggerStay2D(Collider2D other) {
         if(isActive){
            if (other.tag == "Player" ){
                bool unlocked = true;
                foreach(GameObject key in _keys){
                    PlayerInventory inventory = other.GetComponent<PlayerInventory>();
                    ShadowPanel ShadowPanel = key.GetComponent<ShadowPanel>();
                    if( (inventory != null && inventory.ContainGameObject(key)) || ( ShadowPanel != null && ShadowPanel.isActive)){
                        unlocked &= true;
                        
                    }else{
                        unlocked = false;
                        break;
                    }
                }
                if (_doubleCheck){
                    
                    if(unlocked && !_isUnlocked){
                        _isUnlocked = true;
                        _doubleCheck = false;
                        _controller.SetBool("open", true);
                        PlayAudio(_unlockedSound);
                        // StartCoroutine(WaitAndChangeScene(1f));
                        LevelManager.instance.EndLevel(_destination);
                    }else if(unlocked && _isUnlocked){
                        _doubleCheck = false;
                        _isUnlocked = true;
                        LevelManager.instance.EndLevel(_destination);
                        // StartCoroutine(WaitAndChangeScene(.3f));
                    }
                }
             
            }
        }    
    }

    private void LateUpdate() {
                bool unlocked = true;
                foreach(GameObject key in _keys){

                    if(  key.GetComponent<ShadowPanel>() != null && key.GetComponent<ShadowPanel>().isActive){
                        unlocked &= true;
                        
                    }else{
                        unlocked = false;
                        break;
                    }
                }
                if(_doubleCheck){

                    if(unlocked && !_isUnlocked){
                        _isUnlocked = true;
                        _controller.SetBool("open", true);
                        PlayAudio(_unlockedSound);
                    }else if(!unlocked && _isUnlocked){
                        _controller.SetBool("open", false);
                        _isUnlocked = false;
                    }
                }
             
            
    }

    private void PlayAudio(AudioClip sound){
        AudioSource audio = null;
        if(_audio != null){
             audio = new GameObject().AddComponent<AudioSource>();
            _audio = audio;
            _audio.PlayOneShot(sound);
            Destroy(audio.gameObject, 2f);
        }
    }


    // IEnumerator WaitAndChangeScene(float seconds){
    //     yield return new WaitForSeconds(seconds);
    //     MainManager.instance.currentScene = _destination;
    //     SceneManager.LoadScene(_destination);
    // }
}
