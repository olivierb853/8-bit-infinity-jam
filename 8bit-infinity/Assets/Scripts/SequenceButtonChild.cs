﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceButtonChild : MonoBehaviour
{

    private SpriteRenderer _sprite;

    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
    }

    public void Activate(){
        _sprite.color = Color.red;
    }

    public void Deactivate(){
        _sprite.color = Color.white;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        GetComponentInParent<SequenceButton>().PullTrigger(other, gameObject);
    }
}
