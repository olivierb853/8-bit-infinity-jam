﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public Dictionary<string, GameObject> inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = new Dictionary<string, GameObject>();
    }

    public void AddToInventory(string objName, GameObject obj){
        inventory.Add(objName, obj);
    }
    public void RemoveFromInventory(string objName){
        inventory.Remove(objName);
    }

    public bool ContainGameObject(GameObject obj){
        return inventory.ContainsValue(obj);
    }
    public bool ContainObject(string objName){
        return inventory.ContainsKey(objName);
    }
    public string GetObjectKey(GameObject obj){
        foreach (KeyValuePair<string,GameObject> item in inventory){
            if(item.Value == obj){
                return item.Key;
            }
        }
        return null;
    }
}
