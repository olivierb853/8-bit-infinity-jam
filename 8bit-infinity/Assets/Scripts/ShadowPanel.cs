﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowPanel : ShadowWall
{
    // Start is called before the first frame update
        [SerializeField]private AudioClip _activedSound;
        [SerializeField]private AudioSource _audio;
        private bool _activeCheck = false;

    public override void OnTriggerEnter2D(Collider2D other){
        base.OnTriggerEnter2D(other);
        if(!_audio.isPlaying && !isActive && other.tag != "Player"){
            _activeCheck = isActive;
            PlayAudio(_activedSound);
        }

    }

    private void PlayAudio(AudioClip sound){
        _audio.clip = sound;
        _audio.Play();
    }
}
