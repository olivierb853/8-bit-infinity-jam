﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceButton : MonoBehaviour
{
    [SerializeField]private List<GameObject> _buttons;
    private Dictionary<int, SequenceButtonChild> _buttonsIndex = new Dictionary<int, SequenceButtonChild>();
    private int _currentButton = 0;

    void Start()
    {
        int index = 0;
        foreach(GameObject button in _buttons){
            _buttonsIndex.Add(index, button.AddComponent<SequenceButtonChild>());
            index++;
        }
    }

    private void ClearAll(){
        foreach(KeyValuePair<int,SequenceButtonChild> child in _buttonsIndex){
            child.Value.Deactivate();
            _currentButton = 0;
        }
    }

    private void ActivateButton(SequenceButtonChild button){
        button.Activate();
        _currentButton++;
    }

    public void PullTrigger(Collider2D other, GameObject button) {
            Debug.Log("si");
        if(other.tag == "Player" ){
            for(int i = 0; i< _buttons.Count; i++){
                if(_buttonsIndex[i].gameObject == button){
                    if(i > _currentButton){
                        ClearAll();
                    }else{
                        ActivateButton(_buttonsIndex[i]);
                        
                    }
                }
            }
        }
    }
}
