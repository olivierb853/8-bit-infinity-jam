﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitterMouvement : MonoBehaviour
{
    private Rigidbody2D _rb;
    [SerializeField]private AudioClip _sound1;
    [SerializeField]private AudioClip _sound2;
    private Vector2 _velocity;
    [SerializeField]private GameObject _collisionPart; 
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // _velocity = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
        // Vector2 mousePos = Camera.main.ViewportToScreenPoint(Input.mousePosition);


        // _velocity = (mousePos - );
        
    }
    private void FixedUpdate() {
    Vector3 mouse = Input.mousePosition;
     Ray castPoint = Camera.main.ScreenPointToRay(mouse);
     RaycastHit hit;
     if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
     {
        //  _rb.position = Vector2.MoveTowards((Vector2)transform.position,hit.point, Time.deltaTime * 30 );
         _rb.velocity = (hit.point - transform.position).normalized * Time.fixedDeltaTime * 3000;
     }
    //    _rb.position = Vector2.MoveTowards((Vector2)transform.position,Camera.main.ScreenToViewportPoint(Input.mousePosition), Time.deltaTime );
        // _rb.velocity = _velocity * 10f;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(Vector2.Distance(_rb.velocity, Vector2.one) > 1000 * Time.deltaTime){
        
            float particleDuration = _collisionPart.GetComponent<ParticleSystem>().main.duration;
            GameObject partInst = Instantiate(_collisionPart, other.contacts[0].point, Quaternion.identity);
            Destroy(partInst, particleDuration + .5f);
            PlayAudio((2 == Random.Range(1,3)?_sound1:_sound2 ));
        }
    }
    
    private void PlayAudio(AudioClip sound){
        if(sound == null){
            return;
        }
        AudioSource audio = new GameObject().AddComponent<AudioSource>();
        audio.volume = .5f; 
        audio.PlayOneShot(sound);
        Destroy(audio.gameObject, 1f);
    }
}
