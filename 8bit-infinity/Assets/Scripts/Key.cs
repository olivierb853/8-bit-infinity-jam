﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{

    [SerializeField]private AudioClip _grabSound;
    [SerializeField]private AudioSource _audio;
    private bool _grabbed = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag == "Player" && GetComponent<ShadowWall>().isActive && !_grabbed){
            PlayAudio(_grabSound);
            other.GetComponent<PlayerInventory>().AddToInventory(name, gameObject);
            gameObject.SetActive(false);
            _grabbed = true;
        }
    }
    private void PlayAudio(AudioClip sound){
        AudioSource audio = new GameObject().AddComponent<AudioSource>();
        audio.PlayOneShot(sound);
        Destroy(audio.gameObject, 2f);
        // _audio.PlayOneShot(sound);
    }
}
