﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour
{
    public static MainManager instance;

    public string currentLevel;
    [SerializeField]private AudioClip _menuSound;
    [SerializeField]private AudioClip _gameSound;
    private AudioSource _audio;
    private bool _wasMenu;
    private bool _audioIsPlaying = false;
    public string currentScene = Utils.SCENE_MENU;
    
    private void Awake(){
        if(instance == null){
            instance = this;
        }else {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    private void Start(){
        _audio = GetComponent<AudioSource>();
       Button[] buttons = FindObjectsOfType<Button>();
       if(buttons != null){
           foreach(Button button in buttons){
           }
       }
    }

    private void Update() {
        if(Input.GetKeyDown("r") && currentScene != Utils.SCENE_MENU){
            LoadScene(currentScene);
        }    
    }

    public void LoadScene(string destination){
        // Debug.Log("NEXT");
        if(destination != Utils.SCENE_MENU){
            if(!_audioIsPlaying){
                PlayAudio(_gameSound);
                _audioIsPlaying = true;
            }
            _wasMenu = true;
        }else if(_wasMenu){
            PlayAudio(_menuSound);
            _audioIsPlaying = false;
            _wasMenu = false;

        }
        currentScene = destination;
        SceneManager.LoadScene(destination);
    }
    

    private void PlayAudio(AudioClip sound){
        _audio.clip = sound;
        _audio.Play();
    }
    public void PlayAudioInst(AudioClip sound){
         AudioSource audio = new GameObject().AddComponent<AudioSource>();
         audio.gameObject.transform.parent = transform;
        audio.PlayOneShot(sound);
        Destroy(audio.gameObject, 1f);
    }
}
