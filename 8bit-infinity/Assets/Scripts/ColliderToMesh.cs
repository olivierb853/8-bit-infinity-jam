﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 [RequireComponent(typeof(PolygonCollider2D))]
 [RequireComponent(typeof(MeshFilter))]
 [RequireComponent(typeof(MeshRenderer))]
 [ExecuteInEditMode]
public class ColliderToMesh : MonoBehaviour
{
    // Start is called before the first frame update
    private PolygonCollider2D pc2;
    Mesh _mesh;
    MeshFilter _mf;
    int pointCount = 0;
    void Start () {

        pc2 = gameObject.GetComponent<PolygonCollider2D>();
        pointCount = 0;
        pointCount = pc2.GetTotalPointCount();
        _mf = GetComponent<MeshFilter>();
        _mesh = new Mesh();

       Vector2[] points = pc2.points;
        Vector3[] vertices = new Vector3[pointCount];
         Vector2[] uv = new Vector2[pointCount];

         for(int j=0; j<pointCount; j++){
             Vector2 actual = points[j];
             vertices[j] = new Vector3(actual.x, actual.y, 0);
             uv[j] = actual;
         }
        Triangulator tr = new Triangulator(points);
        int [] triangles = tr.Triangulate();
        _mesh.vertices = vertices;
         _mesh.triangles = triangles;
         _mesh.uv = uv;
         _mf.mesh = _mesh;
         _mesh.RecalculateBounds();
       
     
    }

    // Update is called once per frame
    void Update()
    {
  
    }
}
