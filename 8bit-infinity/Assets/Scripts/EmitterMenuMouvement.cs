﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitterMenuMouvement : MonoBehaviour
{
    private Rigidbody2D _rb;
    private Vector2 _velocity;
    [SerializeField]private GameObject _collisionPart; 
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // _velocity = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
        // Vector2 mousePos = Camera.main.ViewportToScreenPoint(Input.mousePosition);


        // _velocity = (mousePos - );
        
    }
    private void FixedUpdate() {
    Vector3 mouse = Input.mousePosition;
     Ray castPoint = Camera.main.ScreenPointToRay(mouse);
     RaycastHit hit;
     if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
     {
         _rb.position = Vector2.MoveTowards((Vector2)transform.position,new Vector2(transform.position.x,hit.point.y), Time.deltaTime * 20 );
     }
    //    _rb.position = Vector2.MoveTowards((Vector2)transform.position,Camera.main.ScreenToViewportPoint(Input.mousePosition), Time.deltaTime );
        // _rb.velocity = _velocity * 10f;
    }

    
    private void OnCollisionEnter2D(Collision2D other) {
        float particleDuration = _collisionPart.GetComponent<ParticleSystem>().main.duration;
        GameObject partInst = Instantiate(_collisionPart, other.contacts[0].point, Quaternion.identity);
        Destroy(partInst, particleDuration + .5f);
    }
}
