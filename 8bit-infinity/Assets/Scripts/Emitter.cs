﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Emitter : MonoBehaviour
{
    // [SerializeField]private GameObject _shadowPolygon;
    [SerializeField]private GameObject _triangleRef;
    [SerializeField]private GameObject _lineRendererRef;
    [SerializeField]private LayerMask _mask;
    private CapsuleCollider2D _playerCollider;

    private string RAYWALLS = "RayWalls";
    private string RAYOBJECTS = "RayObjects";
    private GameObject[] _shadowWalls;
    private Dictionary<PolygonCollider2D, Vector2[]> _verticlesList;
    private List<LineRenderer> _lineRenderers;
    private List<PolygonCollider2D> _polygoneColliders;
    private Vector2 _rayOffset = new Vector2(0.01f, 0.01f);


    void Start(){
        // _playerCollider = GameObject.FindWithTag("Player")?.GetComponent<CapsuleCollider2D>();
        _lineRenderers = new List<LineRenderer>();
        _verticlesList = new Dictionary< PolygonCollider2D, Vector2[]>();
        _polygoneColliders = new List<PolygonCollider2D>();

        GameObject[] rayWalls = GameObject.FindGameObjectsWithTag(RAYWALLS);
        GameObject[] rayObjects = GameObject.FindGameObjectsWithTag(RAYOBJECTS);
        _shadowWalls = rayObjects.Concat(rayWalls).ToArray() ;

        foreach (GameObject objet in _shadowWalls){
            PolygonCollider2D collider = objet.GetComponent<PolygonCollider2D>();
            if(collider != null){
                Vector2[] verticles = collider.points;
                _verticlesList.Add(collider, verticles);
               foreach (Vector2 point in verticles){
                    for (int i = 0; i < 3; i++){     
                        PolygonCollider2D obj = Instantiate(_triangleRef,transform.position, transform.rotation).GetComponent<PolygonCollider2D>();
                        _polygoneColliders.Add(obj);
                    }
               }
            }
        }
      
    }

    void Update(){
        RayToPoints(_verticlesList);
    }

    private void RayToPoints(Dictionary<PolygonCollider2D, Vector2[]> pointsList){
        Dictionary<Dictionary<string, Vector2>, float> posAngle = new Dictionary<Dictionary<string, Vector2>, float>();

        foreach (PolygonCollider2D objet in pointsList.Keys){
            foreach(Vector2 point in pointsList[objet]){

                Vector2 verticle = objet.transform.TransformPoint(point);
                Vector2 direction = verticle - (Vector2)transform.position;

                for(int i = -1 ; i < 2; i++ ) {
                    RaycastHit2D Ray = Physics2D.Raycast(transform.position, direction + _rayOffset * i, Mathf.Infinity, _mask);
                try {
                    //TODO: changer le Dictionaire pour un array .... ALLEGER LE TOUT
                    Dictionary<string, Vector2> dict1 = new Dictionary<string, Vector2>(); 
                    dict1.Add(Ray.transform.name, Ray.point);
                    posAngle.Add(dict1, AngleBetweenVector2( Ray.point - (Vector2)transform.position, Vector2.up));
                }catch(Exception e){
                    Debug.Log(e.StackTrace);
                }
                Debug.DrawRay(transform.position, Ray.point - (Vector2)transform.position, Color.yellow);

                }
            }
        }

        Vector2 firstPoint = new Vector2();
        Vector2 lastPoint = new Vector2();
        int id = 0; 
        foreach (KeyValuePair<Dictionary<string, Vector2>,float> item in posAngle.OrderBy(key => key.Value)){
            if(lastPoint == Vector2.zero){
                firstPoint = lastPoint = item.Key.First().Value;
            }else{
                ChangeColliderPointPos(_polygoneColliders[id], new Vector3[]{transform.position, lastPoint, item.Key.First().Value});
                Debug.DrawRay(item.Key.First().Value, lastPoint- item.Key.First().Value , Color.yellow);
                lastPoint = item.Key.First().Value;
                id++;
            }
        }
        
        ChangeColliderPointPos(_polygoneColliders[id], new Vector3[]{transform.position, lastPoint, firstPoint});
        Debug.DrawRay(firstPoint, lastPoint- firstPoint , Color.yellow);
    }

    private void ChangeColliderPointPos(PolygonCollider2D collider, Vector3[] positions){
        Vector2[] newPointsPos = new Vector2[positions.Count()];
        for(int i = 0; i < positions.Count(); i++){
            newPointsPos[i] = positions[i] - collider.transform.position;
        }
       collider.points = newPointsPos;
    }     

    float AngleBetweenVector2(Vector2 vec1, Vector2 vec2){
        Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
        float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
        return Vector2.Angle(vec1, vec2) * sign;
    }
    
}
