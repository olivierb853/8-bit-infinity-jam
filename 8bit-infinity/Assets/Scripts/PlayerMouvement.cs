using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouvement : MonoBehaviour
{
    [SerializeField]private PlayerController _controller;
    [SerializeField]private float _speed = 10f;
    [SerializeField]private AudioClip _walkingSound;
    [SerializeField]private AudioClip _jumpingSound;
    [SerializeField]private AudioSource _audio;
    [SerializeField]private GameObject _walkingPart;
    [SerializeField]private Transform _groundCheck;
    // [SerializeField]private List<GameObject> _toDestroy = new List<GameObject>();

    private Vector2 _lastPos;
    private Rigidbody2D _rb;
    private Vector2 _velocity;
    private Animator _anim;
    private float _horizontal;
    private bool _jump;
    private float _randPart = .4f;
    // Start is called before the first frame update
    void Start()
    {
        _lastPos = transform.position;
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update() {
        _horizontal = Input.GetAxisRaw("Horizontal") * _speed;
        _anim.SetFloat("horizontal", Mathf.Abs(_horizontal));
       
        if(_horizontal != 0){
            // PlayAudio(_walkingSound);
            if(Vector2.Distance(_lastPos, transform.position) >= _randPart && _controller.IsGrounded && !_jump){
                _randPart = Random.Range(1f,4f);
                _lastPos = transform.position;
                PlayParticles();
            }
        }
        if(Input.GetButtonDown("Jump") || Input.GetKeyDown("w")){
            _anim.SetTrigger("jump");
            PlayAudio(_jumpingSound);
            _jump = true;
        }
    //   if(_toDestroy.Count > 0){
    //       foreach (GameObject objet in _toDestroy)
    //       {  
    //         Destroy(objet, .5f);
    //       }
    //   }
    }
    private void FixedUpdate() {
      
        _controller.Move(_horizontal * Time.deltaTime, false, _jump);
        _jump = false;
    }

    private void PlayParticles() {
        float particleDuration = _walkingPart.GetComponent<ParticleSystem>().main.duration;
        GameObject partInst = Instantiate(_walkingPart, _groundCheck.position + new Vector3(.4f,.01f) * Mathf.Sign(-transform.localScale.x), Quaternion.identity);
        Destroy(partInst, particleDuration + .5f);
    }

    private void PlayAudio(AudioClip sound){
        if(sound == null){
            return;
        }
        AudioSource audio = new GameObject().AddComponent<AudioSource>();
        audio.PlayOneShot(sound);
        Destroy(audio.gameObject, 1f);
    }
}
