﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowWall : MonoBehaviour
{

    [SerializeField]protected bool _shadow;
    protected CircleCollider2D _circleCollider;
    protected BoxCollider2D _boxCollider;
    protected Animator _controller;
    [SerializeField]private List<Collider2D> _otherColliders;
    
    protected SpriteRenderer _renderer;
    // Start is called before the first frame update
    private bool _isActive;
    public bool isActive{
        get{return _isActive ;}
        set{
            if(_controller != null){
                _controller.SetBool("active", value);
            }
            _isActive = value;
            }
    }
    void Start()
    {
        _controller = GetComponent<Animator>();
        _otherColliders = new List<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        if(_renderer == null){
            _renderer = GetComponentInParent<SpriteRenderer>();
        }
        _circleCollider = GetComponent<CircleCollider2D>();
        _boxCollider = GetComponent<BoxCollider2D>();
        _renderer.maskInteraction = (_shadow)?SpriteMaskInteraction.VisibleOutsideMask:SpriteMaskInteraction.VisibleInsideMask;
    }

    // Update is called once per frame
    void Update()
    {
    //    ToggleWall( (_shadow)?  _otherColliders.Count < 1: _otherColliders.Count > 0);
        try{
            foreach (Collider2D other in _otherColliders){
                if(!_circleCollider.IsTouching(other)){
                        _otherColliders.Remove(other);

                    }
            }
        }catch(Exception e){
        }
        
         int _otherLength = _otherColliders.Count;
        ToggleWall( (_shadow)?  _otherLength < 1: _otherLength > 0);
    }

    public virtual void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Rays" && _circleCollider.IsTouching(other)){
            // Debug.Log("ACTIVE");
            if(!_otherColliders.Contains(other)){
                _otherColliders.Add(other);
            }
        }
    }
    // private void OnTriggerExit2D(Collider2D other) {
    //     if(other.tag == "Rays"){
    //        if(_otherColliders.Contains(other)){
    //             _otherColliders.Remove(other);
    //         }
    //     }
    // }

    private void ToggleWall(bool active){
        if(_boxCollider != null){
            isActive = _boxCollider.enabled = active; 
        }
    }

}
