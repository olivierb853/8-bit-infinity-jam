﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleScript : MonoBehaviour
{
    // Start is called before the first frame update
    private PolygonCollider2D pc2;
    private Sprite _sprite;
    private Mesh _mesh;
    private MeshFilter _mf;
    private int pointCount = 0;
    void Start () {

        pc2 = gameObject.GetComponent<PolygonCollider2D>();
        pointCount = 0;
        pointCount = pc2.GetTotalPointCount();
        _mf = GetComponent<MeshFilter>();
        _mesh = GetComponent<MeshFilter>().mesh;
        // _sprite = GetComponent<SpriteMask>().sprite;

    }

    // Update is called once per frame
    void Update()
    {
         Vector2[] points = pc2.points;
        Vector3[] vertices = new Vector3[pointCount];
         Vector2[] uv = new Vector2[pointCount];

         for(int j=0; j<pointCount; j++){
             Vector2 actual = points[j];
             vertices[j] = new Vector3(actual.x, actual.y, 0);
             uv[j] = actual;
         }
         Triangulator tr = new Triangulator(points);
        int [] triangles = tr.Triangulate();
        _mesh.vertices = vertices;
         _mesh.triangles = triangles;
         _mesh.uv = uv;
         _mf.mesh = _mesh;
        _mesh.RecalculateBounds();

        // Vector2[] v2 = ConvertArray(vertices);
        //  _sprite.OverrideGeometry(ConvertArray(vertices),ConvertIntArray(triangles));
    
    }

     Vector2[] ConvertArray(Vector3[] v3){
     Vector2 [] v2 = new Vector2[v3.Length];
     for(int i = 0; i <  v3.Length; i++){
         Vector3 tempV3 = v3[i];
         v2[i] = new Vector2(tempV3.x, tempV3.y);
     }
     return v2;
 }
 
     ushort[] ConvertIntArray(int[] v3){
     ushort [] v2 = new ushort[v3.Length];
     for(int i = 0; i <  v3.Length; i++){
         int tempV3 = v3[i];
         v2[i] = (ushort)v3[i];
     }
     return v2;
 }
}
