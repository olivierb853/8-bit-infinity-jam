﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersoAnimatorController : MonoBehaviour
{


    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update() {

        _anim.SetFloat("horizontal", Mathf.Abs(Input.GetAxisRaw("Horizontal")));
        if(Input.GetButtonDown("Jump")){
            _anim.SetTrigger("jump");
        }
      
    }

}
